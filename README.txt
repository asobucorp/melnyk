Melnyk
======

Getting Started (boilerplate Pyramid stuff)
---------------
- Clone this repo into `melnyk`

- Change directory into your newly created project.

    cd melnyk

- Create a Python virtual environment.

    python3 -m venv env

- Upgrade packaging tools.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e ".[testing]"

- Configure the database.

    env/bin/initialize_melnyk_db development.ini

- Run your project's tests.

    env/bin/pytest

- Run your project.

    env/bin/pserve development.ini

## Migrations

To make, apply and auto-generate migrations from models, do the following:

    $ env/bin/pip install alembic
    $ env/bin/alembic init alembic

In `alembic/env.py` change `target_metadata = None` to:

    from melnyk.models.meta import Base
    target_metadata = Base.metadata

Now you can autogenerate migrations from changed/added tables in `models/`.
