from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
)

from .meta import Base


class MyModel(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    value = Column(Integer)

class SiteUser(Base):
    __tablename__ = 'siteuser'
    id = Column(Integer, primary_key=True)
    username = Column(Text)
    password_digest = Column(Text)
    email = Column(Text)

Index('my_index', MyModel.name, unique=True, mysql_length=255)
